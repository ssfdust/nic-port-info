Nic Port Info
---------------------
The crate provides a way to get the link information of the NIC, including
the port type, supported modes.
The crate is based on the ioctl command, so it can only be used on Linux.

## Examples
List all the NICs' port information.
```
use nic_port_info::get_nic_port_info;
let nics_port = get_nic_port_info(None);
for nic_port in nics_port {
    println!("NIC: {}", nic_port.name());
    println!("Port: {}", nic_port.port());
    println!("Supported: {:?}", nic_port.supported());
}
```

Get the port information of the specified NIC.
```
use nic_port_info::get_nic_port_info;
let nics_port = get_nic_port_info(Some("eth0"));
for nic_port in nics_port {
    println!("NIC: {}", nic_port.name());
    println!("Port: {}", nic_port.port());
    println!("Supported: {:?}", nic_port.supported());
}
```

Get the port information of the specified NIC by PortInfo.
```
use nic_port_info::PortInfo;
let nic_port = PortInfo::from_name("eth0").unwrap();
println!("NIC: {}", nic_port.name());
println!("Port: {}", nic_port.port());
println!("Supported: {:?}", nic_port.supported());
```
