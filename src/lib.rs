//! The crate provides a way to get the link information of the NIC, including
//! the port type, supported modes.
//! The crate is based on the ioctl command, so it can only be used on Linux.
//!
//! # Examples
//! List all the NICs' port information.
//! ```
//! use nic_port_info::get_nic_port_info;
//! let nics_port = get_nic_port_info(None);
//! for nic_port in nics_port {
//!     println!("NIC: {}", nic_port.name());
//!     println!("Port: {}", nic_port.port());
//!     println!("Supported: {:?}", nic_port.supported());
//! }
//! ```
//!
//! Get the port information of the specified NIC.
//! ```
//! use nic_port_info::get_nic_port_info;
//! let nics_port = get_nic_port_info(Some("eth0"));
//! for nic_port in nics_port {
//!     println!("NIC: {}", nic_port.name());
//!     println!("Port: {}", nic_port.port());
//!     println!("Supported: {:?}", nic_port.supported());
//! }
//! ```
//!
//! Get the port information of the specified NIC by PortInfo.
//! ```
//! use nic_port_info::PortInfo;
//! let nic_port = PortInfo::from_name("eth0").unwrap();
//! println!("NIC: {}", nic_port.name());
//! println!("Port: {}", nic_port.port());
//! println!("Supported: {:?}", nic_port.supported());
//! ```
#![allow(non_upper_case_globals)]

#[macro_use]
extern crate nix;

mod errors;
mod ethtool_const;
mod internal;
mod settings_parser;

use crate::ethtool_const::*;
use crate::settings_parser::SettingsParser;
use internal::{CmdContext, EthtoolCommnad};

pub use errors::EthtoolError;

/// The port information includes the port type, supported modes.
#[derive(Default, Debug, Clone)]
pub struct PortInfo {
    name: String,
    port: String,
    supported: Vec<String>,
}

impl PortInfo {
    /// Create a PortInfo from the SettingsParser
    pub fn from_settings_parser(name: &str, settings_parser: SettingsParser) -> Self {
        let supported = settings_parser.supported_link_modes();
        PortInfo {
            name: name.to_string(),
            port: settings_parser.port(),
            supported,
        }
    }

    /// Get the name of the NIC
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Get the port type of the NIC
    pub fn port(&self) -> &str {
        &self.port
    }

    /// Get the supported modes of the NIC
    pub fn supported(&self) -> &Vec<String> {
        &self.supported
    }

    /// Create a PortInfo from the NIC name
    pub fn from_name(name: &str) -> Result<Self, EthtoolError> {
        let ctx = CmdContext::new(name)?;
        let port = do_ioctl_get_nic_port(ctx)?;
        Ok(port)
    }
}


/// Use ioctl to get the port information of the NIC
///
/// The main steps are defined in do_ioctl_ethtool_glinksettings() in ethtool.c
fn do_ioctl_get_nic_port(mut ctx: CmdContext) -> Result<PortInfo, EthtoolError> {
    let mut ecmd = EthtoolCommnad::new(ETHTOOL_GLINKSETTINGS)?;
    /* Handshake with kernel to determine number of words for link
     * mode bitmaps. When requested number of bitmap words is not
     * the one expected by kernel, the latter returns the integer
     * opposite of what it is expecting. We request length 0 below
     * (aka. invalid bitmap length) to get this info.
     */
    ctx = ctx.send_ioctl(ecmd)?;
    ecmd = ctx.get_ethtool_link_settings();
    if ecmd.req.link_mode_masks_nwords >= 0 || ecmd.req.cmd != ETHTOOL_GLINKSETTINGS {
        return Err(EthtoolError::new(
            "Failed to determine number of words for link mode bitmaps",
        ));
    }

    /* got the real ecmd.req.link_mode_masks_nwords,
     * now send the real request
     */
    ecmd.req.cmd = ETHTOOL_GLINKSETTINGS;
    ecmd.req.link_mode_masks_nwords = -ecmd.req.link_mode_masks_nwords;
    ctx = ctx.send_ioctl(ecmd)?;
    ecmd = ctx.get_ethtool_link_settings();

    /* check the link_mode_masks_nwords again */
    if ecmd.req.link_mode_masks_nwords <= 0 || ecmd.req.cmd != ETHTOOL_GLINKSETTINGS {
        return Err(EthtoolError::new(
            "Failed to check the link_mode_masks_nwords.",
        ));
    }

    ctx.close_socket();

    Ok(ecmd.into_port(ctx.ifname()))
}


/// Get the port information of the NIC
/// If devname is None, get all the NICs' port information.
/// If devname is Some(&str), get the specified NIC's port information.
pub fn get_nic_port_info(devname: Option<&str>) -> Vec<PortInfo> {
    let mut nics_port = Vec::new();
    if let Some(devname) = devname {
        let port_info = PortInfo::from_name(devname);
        if let Ok(port_info) = port_info {
            nics_port.push(port_info);
        }
    } else {
        let mut add_ports = Vec::new();
        if let Ok(ifaddrs) = nix::ifaddrs::getifaddrs() {
            ifaddrs.for_each(|ifaddr| {
                if let Ok(nic_port) = PortInfo::from_name(&ifaddr.interface_name) {
                    if !add_ports.contains(&nic_port.name) {
                        add_ports.push(nic_port.name.clone());
                        nics_port.push(nic_port);
                    }
                }
            });
        }
    }
    nics_port
}
