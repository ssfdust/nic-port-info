//! Constants for ethtool
//!
//! This file is copied from the c2rust conversion of ethtool.c
pub const ETHTOOL_LINK_MODE_10baseT1S_P2MP_Half_BIT: u32 = 101;
pub const ETHTOOL_LINK_MODE_10baseT1S_Half_BIT: u32 = 100;
pub const ETHTOOL_LINK_MODE_10baseT1S_Full_BIT: u32 = 99;
pub const ETHTOOL_LINK_MODE_800000baseVR8_Full_BIT: u32 = 98;
pub const ETHTOOL_LINK_MODE_800000baseSR8_Full_BIT: u32 = 97;
pub const ETHTOOL_LINK_MODE_800000baseDR8_2_Full_BIT: u32 = 96;
pub const ETHTOOL_LINK_MODE_800000baseDR8_Full_BIT: u32 = 95;
pub const ETHTOOL_LINK_MODE_800000baseKR8_Full_BIT: u32 = 94;
pub const ETHTOOL_LINK_MODE_800000baseCR8_Full_BIT: u32 = 93;
pub const ETHTOOL_LINK_MODE_10baseT1L_Full_BIT: u32 = 92;
pub const ETHTOOL_LINK_MODE_100baseFX_Full_BIT: u32 = 91;
pub const ETHTOOL_LINK_MODE_100baseFX_Half_BIT: u32 = 90;
pub const ETHTOOL_LINK_MODE_400000baseCR4_Full_BIT: u32 = 89;
pub const ETHTOOL_LINK_MODE_400000baseDR4_Full_BIT: u32 = 88;
pub const ETHTOOL_LINK_MODE_400000baseLR4_ER4_FR4_Full_BIT: u32 = 87;
pub const ETHTOOL_LINK_MODE_400000baseSR4_Full_BIT: u32 = 86;
pub const ETHTOOL_LINK_MODE_400000baseKR4_Full_BIT: u32 = 85;
pub const ETHTOOL_LINK_MODE_200000baseCR2_Full_BIT: u32 = 84;
pub const ETHTOOL_LINK_MODE_200000baseDR2_Full_BIT: u32 = 83;
pub const ETHTOOL_LINK_MODE_200000baseLR2_ER2_FR2_Full_BIT: u32 = 82;
pub const ETHTOOL_LINK_MODE_200000baseSR2_Full_BIT: u32 = 81;
pub const ETHTOOL_LINK_MODE_200000baseKR2_Full_BIT: u32 = 80;
pub const ETHTOOL_LINK_MODE_100000baseDR_Full_BIT: u32 = 79;
pub const ETHTOOL_LINK_MODE_100000baseCR_Full_BIT: u32 = 78;
pub const ETHTOOL_LINK_MODE_100000baseLR_ER_FR_Full_BIT: u32 = 77;
pub const ETHTOOL_LINK_MODE_100000baseSR_Full_BIT: u32 = 76;
pub const ETHTOOL_LINK_MODE_100000baseKR_Full_BIT: u32 = 75;
pub const ETHTOOL_LINK_MODE_400000baseCR8_Full_BIT: u32 = 73;
pub const ETHTOOL_LINK_MODE_400000baseDR8_Full_BIT: u32 = 72;
pub const ETHTOOL_LINK_MODE_400000baseLR8_ER8_FR8_Full_BIT: u32 = 71;
pub const ETHTOOL_LINK_MODE_400000baseSR8_Full_BIT: u32 = 70;
pub const ETHTOOL_LINK_MODE_400000baseKR8_Full_BIT: u32 = 69;
pub const ETHTOOL_LINK_MODE_1000baseT1_Full_BIT: u32 = 68;
pub const ETHTOOL_LINK_MODE_100baseT1_Full_BIT: u32 = 67;
pub const ETHTOOL_LINK_MODE_200000baseCR4_Full_BIT: u32 = 66;
pub const ETHTOOL_LINK_MODE_200000baseDR4_Full_BIT: u32 = 65;
pub const ETHTOOL_LINK_MODE_200000baseLR4_ER4_FR4_Full_BIT: u32 = 64;
pub const ETHTOOL_LINK_MODE_200000baseSR4_Full_BIT: u32 = 63;
pub const ETHTOOL_LINK_MODE_200000baseKR4_Full_BIT: u32 = 62;
pub const ETHTOOL_LINK_MODE_100000baseDR2_Full_BIT: u32 = 61;
pub const ETHTOOL_LINK_MODE_100000baseLR2_ER2_FR2_Full_BIT: u32 = 60;
pub const ETHTOOL_LINK_MODE_100000baseCR2_Full_BIT: u32 = 59;
pub const ETHTOOL_LINK_MODE_100000baseSR2_Full_BIT: u32 = 58;
pub const ETHTOOL_LINK_MODE_100000baseKR2_Full_BIT: u32 = 57;
pub const ETHTOOL_LINK_MODE_50000baseDR_Full_BIT: u32 = 56;
pub const ETHTOOL_LINK_MODE_50000baseLR_ER_FR_Full_BIT: u32 = 55;
pub const ETHTOOL_LINK_MODE_50000baseCR_Full_BIT: u32 = 54;
pub const ETHTOOL_LINK_MODE_50000baseSR_Full_BIT: u32 = 53;
pub const ETHTOOL_LINK_MODE_50000baseKR_Full_BIT: u32 = 52;
pub const ETHTOOL_LINK_MODE_5000baseT_Full_BIT: u32 = 48;
pub const ETHTOOL_LINK_MODE_2500baseT_Full_BIT: u32 = 47;
pub const ETHTOOL_LINK_MODE_10000baseER_Full_BIT: u32 = 46;
pub const ETHTOOL_LINK_MODE_10000baseLRM_Full_BIT: u32 = 45;
pub const ETHTOOL_LINK_MODE_10000baseLR_Full_BIT: u32 = 44;
pub const ETHTOOL_LINK_MODE_10000baseSR_Full_BIT: u32 = 43;
pub const ETHTOOL_LINK_MODE_10000baseCR_Full_BIT: u32 = 42;
pub const ETHTOOL_LINK_MODE_1000baseX_Full_BIT: u32 = 41;
pub const ETHTOOL_LINK_MODE_50000baseSR2_Full_BIT: u32 = 40;
pub const ETHTOOL_LINK_MODE_100000baseLR4_ER4_Full_BIT: u32 = 39;
pub const ETHTOOL_LINK_MODE_100000baseCR4_Full_BIT: u32 = 38;
pub const ETHTOOL_LINK_MODE_100000baseSR4_Full_BIT: u32 = 37;
pub const ETHTOOL_LINK_MODE_100000baseKR4_Full_BIT: u32 = 36;
pub const ETHTOOL_LINK_MODE_50000baseKR2_Full_BIT: u32 = 35;
pub const ETHTOOL_LINK_MODE_50000baseCR2_Full_BIT: u32 = 34;
pub const ETHTOOL_LINK_MODE_25000baseSR_Full_BIT: u32 = 33;
pub const ETHTOOL_LINK_MODE_25000baseKR_Full_BIT: u32 = 32;
pub const ETHTOOL_LINK_MODE_25000baseCR_Full_BIT: u32 = 31;
pub const ETHTOOL_LINK_MODE_56000baseLR4_Full_BIT: u32 = 30;
pub const ETHTOOL_LINK_MODE_56000baseSR4_Full_BIT: u32 = 29;
pub const ETHTOOL_LINK_MODE_56000baseCR4_Full_BIT: u32 = 28;
pub const ETHTOOL_LINK_MODE_56000baseKR4_Full_BIT: u32 = 27;
pub const ETHTOOL_LINK_MODE_40000baseLR4_Full_BIT: u32 = 26;
pub const ETHTOOL_LINK_MODE_40000baseSR4_Full_BIT: u32 = 25;
pub const ETHTOOL_LINK_MODE_40000baseCR4_Full_BIT: u32 = 24;
pub const ETHTOOL_LINK_MODE_40000baseKR4_Full_BIT: u32 = 23;
pub const ETHTOOL_LINK_MODE_20000baseKR2_Full_BIT: u32 = 22;
pub const ETHTOOL_LINK_MODE_20000baseMLD2_Full_BIT: u32 = 21;
pub const ETHTOOL_LINK_MODE_10000baseR_FEC_BIT: u32 = 20;
pub const ETHTOOL_LINK_MODE_10000baseKR_Full_BIT: u32 = 19;
pub const ETHTOOL_LINK_MODE_10000baseKX4_Full_BIT: u32 = 18;
pub const ETHTOOL_LINK_MODE_1000baseKX_Full_BIT: u32 = 17;
pub const ETHTOOL_LINK_MODE_Backplane_BIT: u32 = 16;
pub const ETHTOOL_LINK_MODE_2500baseX_Full_BIT: u32 = 15;
pub const ETHTOOL_LINK_MODE_10000baseT_Full_BIT: u32 = 12;
pub const ETHTOOL_LINK_MODE_BNC_BIT: u32 = 11;
pub const ETHTOOL_LINK_MODE_FIBRE_BIT: u32 = 10;
pub const ETHTOOL_LINK_MODE_MII_BIT: u32 = 9;
pub const ETHTOOL_LINK_MODE_AUI_BIT: u32 = 8;
pub const ETHTOOL_LINK_MODE_TP_BIT: u32 = 7;
pub const ETHTOOL_LINK_MODE_1000baseT_Full_BIT: u32 = 5;
pub const ETHTOOL_LINK_MODE_1000baseT_Half_BIT: u32 = 4;
pub const ETHTOOL_LINK_MODE_100baseT_Full_BIT: u32 = 3;
pub const ETHTOOL_LINK_MODE_100baseT_Half_BIT: u32 = 2;
pub const ETHTOOL_LINK_MODE_10baseT_Full_BIT: u32 = 1;
pub const ETHTOOL_LINK_MODE_10baseT_Half_BIT: u32 = 0;
pub const ETHTOOL_GLINKSETTINGS: u32 = 0x0000004C;
pub const IFNAMSIZ: usize = 16;
pub const ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NU32: usize = i8::MAX as usize;
pub const ETHTOOL_LINK_MODE_MASK_MAX_KERNEL_NBITS: u32 = 32 * i8::MAX as u32;

pub const ETHTOOL_PORT_BIT_MODES: [(u32, &str); 6] = [
    (ETHTOOL_LINK_MODE_TP_BIT, "TP"),
    (ETHTOOL_LINK_MODE_AUI_BIT, "AUI"),
    (ETHTOOL_LINK_MODE_MII_BIT, "MII"),
    (ETHTOOL_LINK_MODE_BNC_BIT, "BNC"),
    (ETHTOOL_LINK_MODE_FIBRE_BIT, "FIBRE"),
    (ETHTOOL_LINK_MODE_Backplane_BIT, "BACKPLANE"),
];

pub const ETHTOOL_SUPPORTED_BIT_MODES: [(u32, &str); 89] = [
    (ETHTOOL_LINK_MODE_10baseT_Half_BIT, "10baseT/Half"),
    (ETHTOOL_LINK_MODE_10baseT_Full_BIT, "10baseT/Full"),
    (ETHTOOL_LINK_MODE_100baseT_Half_BIT, "100baseT/Half"),
    (ETHTOOL_LINK_MODE_100baseT_Full_BIT, "100baseT/Full"),
    (ETHTOOL_LINK_MODE_1000baseT_Half_BIT, "1000baseT/Half"),
    (ETHTOOL_LINK_MODE_1000baseT_Full_BIT, "1000baseT/Full"),
    (ETHTOOL_LINK_MODE_10000baseT_Full_BIT, "10000baseT/Full"),
    (ETHTOOL_LINK_MODE_2500baseX_Full_BIT, "2500baseX/Full"),
    (ETHTOOL_LINK_MODE_1000baseKX_Full_BIT, "1000baseKX/Full"),
    (ETHTOOL_LINK_MODE_10000baseKX4_Full_BIT, "10000baseKX4/Full"),
    (ETHTOOL_LINK_MODE_10000baseKR_Full_BIT, "10000baseKR/Full"),
    (ETHTOOL_LINK_MODE_10000baseR_FEC_BIT, "10000baseR_FEC"),
    (
        ETHTOOL_LINK_MODE_20000baseMLD2_Full_BIT,
        "20000baseMLD2/Full",
    ),
    (ETHTOOL_LINK_MODE_20000baseKR2_Full_BIT, "20000baseKR2/Full"),
    (ETHTOOL_LINK_MODE_40000baseKR4_Full_BIT, "40000baseKR4/Full"),
    (ETHTOOL_LINK_MODE_40000baseCR4_Full_BIT, "40000baseCR4/Full"),
    (ETHTOOL_LINK_MODE_40000baseSR4_Full_BIT, "40000baseSR4/Full"),
    (ETHTOOL_LINK_MODE_40000baseLR4_Full_BIT, "40000baseLR4/Full"),
    (ETHTOOL_LINK_MODE_56000baseKR4_Full_BIT, "56000baseKR4/Full"),
    (ETHTOOL_LINK_MODE_56000baseCR4_Full_BIT, "56000baseCR4/Full"),
    (ETHTOOL_LINK_MODE_56000baseSR4_Full_BIT, "56000baseSR4/Full"),
    (ETHTOOL_LINK_MODE_56000baseLR4_Full_BIT, "56000baseLR4/Full"),
    (ETHTOOL_LINK_MODE_25000baseCR_Full_BIT, "25000baseCR/Full"),
    (ETHTOOL_LINK_MODE_25000baseKR_Full_BIT, "25000baseKR/Full"),
    (ETHTOOL_LINK_MODE_25000baseSR_Full_BIT, "25000baseSR/Full"),
    (ETHTOOL_LINK_MODE_50000baseCR2_Full_BIT, "50000baseCR2/Full"),
    (ETHTOOL_LINK_MODE_50000baseKR2_Full_BIT, "50000baseKR2/Full"),
    (
        ETHTOOL_LINK_MODE_100000baseKR4_Full_BIT,
        "100000baseKR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseSR4_Full_BIT,
        "100000baseSR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseCR4_Full_BIT,
        "100000baseCR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseLR4_ER4_Full_BIT,
        "100000baseLR4_ER4/Full",
    ),
    (ETHTOOL_LINK_MODE_50000baseSR2_Full_BIT, "50000baseSR2/Full"),
    (ETHTOOL_LINK_MODE_1000baseX_Full_BIT, "1000baseX/Full"),
    (ETHTOOL_LINK_MODE_10000baseCR_Full_BIT, "10000baseCR/Full"),
    (ETHTOOL_LINK_MODE_10000baseSR_Full_BIT, "10000baseSR/Full"),
    (ETHTOOL_LINK_MODE_10000baseLR_Full_BIT, "10000baseLR/Full"),
    (ETHTOOL_LINK_MODE_10000baseLRM_Full_BIT, "10000baseLRM/Full"),
    (ETHTOOL_LINK_MODE_10000baseER_Full_BIT, "10000baseER/Full"),
    (ETHTOOL_LINK_MODE_2500baseT_Full_BIT, "2500baseT/Full"),
    (ETHTOOL_LINK_MODE_5000baseT_Full_BIT, "5000baseT/Full"),
    (ETHTOOL_LINK_MODE_50000baseKR_Full_BIT, "50000baseKR/Full"),
    (ETHTOOL_LINK_MODE_50000baseSR_Full_BIT, "50000baseSR/Full"),
    (ETHTOOL_LINK_MODE_50000baseCR_Full_BIT, "50000baseCR/Full"),
    (
        ETHTOOL_LINK_MODE_50000baseLR_ER_FR_Full_BIT,
        "50000baseLR_ER_FR/Full",
    ),
    (ETHTOOL_LINK_MODE_50000baseDR_Full_BIT, "50000baseDR/Full"),
    (
        ETHTOOL_LINK_MODE_100000baseKR2_Full_BIT,
        "100000baseKR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseSR2_Full_BIT,
        "100000baseSR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseCR2_Full_BIT,
        "100000baseCR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseLR2_ER2_FR2_Full_BIT,
        "100000baseLR2_ER2_FR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_100000baseDR2_Full_BIT,
        "100000baseDR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseKR4_Full_BIT,
        "200000baseKR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseSR4_Full_BIT,
        "200000baseSR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseLR4_ER4_FR4_Full_BIT,
        "200000baseLR4_ER4_FR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseDR4_Full_BIT,
        "200000baseDR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseCR4_Full_BIT,
        "200000baseCR4/Full",
    ),
    (ETHTOOL_LINK_MODE_100baseT1_Full_BIT, "100baseT1/Full"),
    (ETHTOOL_LINK_MODE_1000baseT1_Full_BIT, "1000baseT1/Full"),
    (
        ETHTOOL_LINK_MODE_400000baseKR8_Full_BIT,
        "400000baseKR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseSR8_Full_BIT,
        "400000baseSR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseLR8_ER8_FR8_Full_BIT,
        "400000baseLR8_ER8_FR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseDR8_Full_BIT,
        "400000baseDR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseCR8_Full_BIT,
        "400000baseCR8/Full",
    ),
    (ETHTOOL_LINK_MODE_100000baseKR_Full_BIT, "100000baseKR/Full"),
    (ETHTOOL_LINK_MODE_100000baseSR_Full_BIT, "100000baseSR/Full"),
    (
        ETHTOOL_LINK_MODE_100000baseLR_ER_FR_Full_BIT,
        "100000baseLR_ER_FR/Full",
    ),
    (ETHTOOL_LINK_MODE_100000baseDR_Full_BIT, "100000baseDR/Full"),
    (ETHTOOL_LINK_MODE_100000baseCR_Full_BIT, "100000baseCR/Full"),
    (
        ETHTOOL_LINK_MODE_200000baseKR2_Full_BIT,
        "200000baseKR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseSR2_Full_BIT,
        "200000baseSR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseLR2_ER2_FR2_Full_BIT,
        "200000baseLR2_ER2_FR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseDR2_Full_BIT,
        "200000baseDR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_200000baseCR2_Full_BIT,
        "200000baseCR2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseKR4_Full_BIT,
        "400000baseKR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseSR4_Full_BIT,
        "400000baseSR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseLR4_ER4_FR4_Full_BIT,
        "400000baseLR4_ER4_FR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseDR4_Full_BIT,
        "400000baseDR4/Full",
    ),
    (
        ETHTOOL_LINK_MODE_400000baseCR4_Full_BIT,
        "400000baseCR4/Full",
    ),
    (ETHTOOL_LINK_MODE_100baseFX_Half_BIT, "100baseFX/Half"),
    (ETHTOOL_LINK_MODE_100baseFX_Full_BIT, "100baseFX/Full"),
    (ETHTOOL_LINK_MODE_10baseT1L_Full_BIT, "10baseT1L/Full"),
    (
        ETHTOOL_LINK_MODE_800000baseCR8_Full_BIT,
        "800000baseCR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_800000baseKR8_Full_BIT,
        "800000baseKR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_800000baseDR8_Full_BIT,
        "800000baseDR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_800000baseDR8_2_Full_BIT,
        "800000baseDR8_2/Full",
    ),
    (
        ETHTOOL_LINK_MODE_800000baseSR8_Full_BIT,
        "800000baseSR8/Full",
    ),
    (
        ETHTOOL_LINK_MODE_800000baseVR8_Full_BIT,
        "800000baseVR8/Full",
    ),
    (ETHTOOL_LINK_MODE_10baseT1S_Full_BIT, "10baseT1S/Full"),
    (ETHTOOL_LINK_MODE_10baseT1S_Half_BIT, "10baseT1S/Half"),
    (ETHTOOL_LINK_MODE_10baseT1S_P2MP_Half_BIT, "10baseT1S/Half"),
];
