//! Get interface information by given interface name
use nic_port_info::get_nic_port_info;

fn main() {
    let interface = std::env::args().nth(1);
    let nic_port_info = get_nic_port_info(interface.as_deref());
    for nic in nic_port_info {
        println!(
            "Interface: {}, Port: {}, Supported: {}",
            nic.name(),
            nic.port(),
            nic.supported().join(", ")
        );
    }
}
