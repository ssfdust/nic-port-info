//! List information of all interfaces
use nic_port_info::get_nic_port_info;

fn main() {
    let nic_port_info = get_nic_port_info(None);
    for nic in nic_port_info {
        println!(
            "Interface: {}, Port: {}, Supported: {}",
            nic.name(),
            nic.port(),
            nic.supported().join(", ")
        );
    }
}
